package com.example.theo.detecter_objects.adapter;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.theo.detecter_objects.R;
import com.example.theo.detecter_objects.model.ProductModel;

import java.util.ArrayList;

public class ProductSearchImageAdapter extends RecyclerView.Adapter<ProductSearchImageAdapter.ViewHolderProducto> implements View.OnClickListener {
    private ArrayList<ProductModel> listaProducto;
    private View.OnClickListener listener;

    public ProductSearchImageAdapter(ArrayList<ProductModel> listaProducto) {
        this.listaProducto = listaProducto;
    }

    @Override
    public ViewHolderProducto onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        view.setOnClickListener(this);

        return new ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderProducto holder, int position) {
        holder.txtNombreProducto.setText(listaProducto.get(position).getNombre());
        String strPrecio = "S/." + listaProducto.get(position).getPrecio().toString();
        holder.txtPrecioProducto.setText(strPrecio);
        String strPrecioTachado = "S/." + listaProducto.get(position).getPrecioTachado();
        holder.txtPrecioTachado.setText(strPrecioTachado);
        holder.txtPrecioTachado.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.imgProducto.setImageResource(listaProducto.get(position).getImagen());
    }

    @Override
    public int getItemCount() {
        return listaProducto.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public class ViewHolderProducto extends RecyclerView.ViewHolder {

        ImageView imgProducto;
        TextView txtNombreProducto;
        TextView txtPrecioProducto;
        TextView txtPrecioTachado;

        public ViewHolderProducto(View itemView) {
            super(itemView);

            imgProducto = itemView.findViewById(R.id.imgProductoHome);
            txtNombreProducto = itemView.findViewById(R.id.txtNombreProductoHome);
            txtPrecioProducto = itemView.findViewById(R.id.txtPrecioProductoHome);
            txtPrecioTachado = itemView.findViewById(R.id.txtPrecioProductoTachadoHome);
        }
    }
}
