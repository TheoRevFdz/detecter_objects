package com.example.theo.detecter_objects.model;

public class ProductModel {

    private String id;
    private String nombre;
    private String descripcion;
    private Double precio;
    private Double precioTachado;
    private int imagen;

    public ProductModel() {
    }

    public ProductModel(String id, String nombre, String descripcion, Double precio, Double precioTachado, int imagen) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.precioTachado = precioTachado;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getPrecioTachado() {
        return precioTachado;
    }

    public void setPrecioTachado(Double precioTachado) {
        this.precioTachado = precioTachado;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}
