package com.example.theo.detecter_objects.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class PermissionManager {

    public static boolean permisoCamara(Activity activity, String tag, int codigoRespuesta) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(tag, "Permiso otorgado");
                return true;
            } else {
                Log.v(tag, "El permiso es revocado\n");
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, codigoRespuesta);
                return false;
            }
        } else {
            Log.v(tag, "Permiso se concede");
            return true;
        }
    }
}
